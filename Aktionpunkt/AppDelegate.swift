//
//  AppDelegate.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 6.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import Firebase
import FirebaseMessaging
import UserNotifications
import UserNotificationsUI
import FirebaseInstanceID

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id" // firebase
    static var isNofitication = false
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        // register_for_notifications
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications() // if else ten sonra olmali.

        
        // MARK: Firebase Configure
        FirebaseApp.configure()
        
        // Uygulama bildirim sayisini sifirlama
        application.applicationIconBadgeNumber = 0
        let current = UNUserNotificationCenter.current()
        current.removeAllDeliveredNotifications() // To remove all delivered notifications
        current.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self //as? MessagingDelegate
        // [END set_messaging_delegate]
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
    
    // MARK: Firebase Triggers
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed. Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // uygulama acikken bildirimi yakalar
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (_ options: UNNotificationPresentationOptions) -> Void) {
//        print(notification.request.content.userInfo)
//        let baslik = notification.request.content.userInfo["baslik"] as! String
//        let mesaj = notification.request.content.userInfo["mesaj"] as! String
//        let tarih = notification.request.content.userInfo["tarih"] as! String
//
//        print("baslik:\(baslik) + mesaj:\(mesaj) + tarih:\(tarih)")
//        AlertFunctions.messageType.showOKAlert(baslik, bodyMessage: mesaj)

        let dict = notification.request.content.userInfo["aps"] as! NSDictionary
        if let d : [String : Any] = dict["alert"] as? [String : Any] {
            print(dict)
            let body : String = d["body"] as! String
            let title : String = d["title"] as! String
            print("Title:\(title) + body:\(body)")
            AlertFunctions.messageType.showOKAlert(body, bodyMessage: title)

        } else {
            let dict = notification.request.content.userInfo["aps"] as! NSDictionary;
            print(dict)
            let message = dict["alert"];
            print("%@", message!);
            AlertFunctions.messageType.showOKAlert("", bodyMessage: message! as! String)
        }
    }
    
    // MARK: [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }

        application.applicationIconBadgeNumber += 1
        
        let dict = userInfo["aps"] as! NSDictionary
        if let d : [String : Any] = dict["alert"] as? [String : Any] {
            let body : String = d["body"] as! String
            let title : String = d["title"] as! String
            print("Title:\(title) + body:\(body)")
            AlertFunctions.messageType.showOKAlert(body, bodyMessage: title)

        } else {
            let dict = userInfo["aps"] as! NSDictionary;
            print(dict)
            let message = dict["alert"];
            print("%@", message!);
            AlertFunctions.messageType.showOKAlert("", bodyMessage: message! as! String)
        }

    }
    
    // MARK: Uygulama kapali iken bildirime tiklaninca acilirken buraya girer
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.

        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
//        print(userInfo)
//        let baslik = userInfo["baslik"] as! String
//        let mesaj = userInfo["mesaj"] as! String
//        let tarih = userInfo["tarih"] as! String
//
//        print("baslik:\(baslik) + mesaj:\(mesaj) + tarih:\(tarih)")
//        AlertFunctions.messageType.showOKAlert(baslik, bodyMessage: mesaj)
        
        let dict = userInfo["aps"] as! NSDictionary
        if let d : [String : Any] = dict["alert"] as? [String : Any] {
            let body : String = d["body"] as! String
            let title : String = d["title"] as! String
            print("Title:\(title) + body:\(body)")

        } else {
            let dict = userInfo["aps"] as! NSDictionary;
            print(dict)
            let message = dict["alert"];
            print("%@", message!);
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
        
        AppDelegate.isNofitication = true
        // Open Bildirimlerim
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BildirimView")
        GlobalVariables.getTopController().addChildViewController(vc)
        vc.view.frame = GlobalVariables.getTopController().view.frame
        GlobalVariables.getTopController().view.addSubview(vc.view)
        vc.didMove(toParentViewController: GlobalVariables.getTopController())
        
    }
    // [END receive_message]
    
//    func application(_ application: UIApplication,
//                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken
//        print("token", deviceToken)
//    }
    
}

extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
    }
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
    // [END ios_10_data_message]
}

