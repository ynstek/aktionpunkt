//
//  JsonBildirimler.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 20.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonBildirimler : NSObject  {
    
    struct Value : Codable {
        let Id : Int?
        let Baslik : String?
        let Mesaj : String?
        let EklemeTarihi : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            Baslik = try values.decodeIfPresent(String.self, forKey: .Baslik)
            Mesaj = try values.decodeIfPresent(String.self, forKey: .Mesaj)
            EklemeTarihi = try values.decodeIfPresent(String.self, forKey: .EklemeTarihi)
        }
    }
    
    static func endpointForTodos() -> String {
        var url = GlobalVariables.shared.apiURL + "bildirim/bildirimList"
        
        url = url.replacingOccurrences(of: " ", with: "%20")
        return url
    }
    
    struct Todo: Codable {
        // GET ALL TODOS
        static func allTodos(completionHandler: @escaping ([Value]?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                //                let statuscode = (response as! HTTPURLResponse).statusCode
                
                guard let responseData = data else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    let todos = try JSONDecoder().decode([Value].self, from: responseData)
                    
                    completionHandler(todos, nil)
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
}


