//
//  JsonUrun.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 13.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonUrun : NSObject  {
    
    struct UrunResimleris : Codable {
        let id : Int?
        let urunID : Int?
        let resimYolu : String?
        let sirasi : Int?
        let eklenmeTarihi : String?
        let urunler : String?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "Id"
            case urunID = "UrunID"
            case resimYolu = "ResimYolu"
            case sirasi = "Sirasi"
            case eklenmeTarihi = "EklenmeTarihi"
            case urunler = "Urunler"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            urunID = try values.decodeIfPresent(Int.self, forKey: .urunID)
            resimYolu = try values.decodeIfPresent(String.self, forKey: .resimYolu)
            sirasi = try values.decodeIfPresent(Int.self, forKey: .sirasi)
            eklenmeTarihi = try values.decodeIfPresent(String.self, forKey: .eklenmeTarihi)
            urunler = try values.decodeIfPresent(String.self, forKey: .urunler)
        }
        
    }
    
    
    struct Value : Codable {
        let id : Int?
        let firmaID : Int?
        let adi : String?
        let urunResmi : String?
        let kisaAciklama : String?
        let uzunAciklama : String?
        let birimFiyat : Double?
        let indirimliFiyat : Double?
        let indirimOrani : Int?
        let indirimliMi : Bool?
        let sliderdaGosterilsinMi : String?
        let paraBirimi : String?
        let sirasi : Int?
        let eklemeTarihi : String?
        let firmalar : String?
        let urunResimleris : [UrunResimleris]?
        
        enum CodingKeys: String, CodingKey {
            
            case id = "Id"
            case firmaID = "FirmaID"
            case adi = "Adi"
            case urunResmi = "UrunResmi"
            case kisaAciklama = "KisaAciklama"
            case uzunAciklama = "UzunAciklama"
            case birimFiyat = "BirimFiyat"
            case indirimliFiyat = "IndirimliFiyat"
            case indirimOrani = "IndirimOrani"
            case indirimliMi = "IndirimliMi"
            case sliderdaGosterilsinMi = "SliderdaGosterilsinMi"
            case paraBirimi = "ParaBirimi"
            case sirasi = "Sirasi"
            case eklemeTarihi = "EklemeTarihi"
            case firmalar = "Firmalar"
            case urunResimleris = "UrunResimleris"
        }
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            id = try values.decodeIfPresent(Int.self, forKey: .id)
            firmaID = try values.decodeIfPresent(Int.self, forKey: .firmaID)
            adi = try values.decodeIfPresent(String.self, forKey: .adi)
            urunResmi = try values.decodeIfPresent(String.self, forKey: .urunResmi)
            kisaAciklama = try values.decodeIfPresent(String.self, forKey: .kisaAciklama)
            uzunAciklama = try values.decodeIfPresent(String.self, forKey: .uzunAciklama)
            birimFiyat = try values.decodeIfPresent(Double.self, forKey: .birimFiyat)
            indirimliFiyat = try values.decodeIfPresent(Double.self, forKey: .indirimliFiyat)
            indirimOrani = try values.decodeIfPresent(Int.self, forKey: .indirimOrani)
            indirimliMi = try values.decodeIfPresent(Bool.self, forKey: .indirimliMi)
            sliderdaGosterilsinMi = try values.decodeIfPresent(String.self, forKey: .sliderdaGosterilsinMi)
            paraBirimi = try values.decodeIfPresent(String.self, forKey: .paraBirimi)
            sirasi = try values.decodeIfPresent(Int.self, forKey: .sirasi)
            eklemeTarihi = try values.decodeIfPresent(String.self, forKey: .eklemeTarihi)
            firmalar = try values.decodeIfPresent(String.self, forKey: .firmalar)
            urunResimleris = try values.decodeIfPresent([UrunResimleris].self, forKey: .urunResimleris)
        }
        
    }
    
    static func endpointForTodos(_ firmaId: Int) -> String {
        var url = GlobalVariables.shared.apiURL + "urun/urunList?firmaID=\(firmaId)"
        
        url = url.replacingOccurrences(of: " ", with: "%20")
        return url
    }
    
    struct Todo: Codable {
        // GET ALL TODOS
        static func allTodos(_ firmaId: Int, completionHandler: @escaping ([Value]?, Error?) -> Void) {
            let endpoint = endpointForTodos(firmaId)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                //                let statuscode = (response as! HTTPURLResponse).statusCode
                
                guard let responseData = data else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    let todos = try JSONDecoder().decode([Value].self, from: responseData)
                    
                    completionHandler(todos, nil)
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
}

