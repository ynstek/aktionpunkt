//
//  JsonFeedback.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 16.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonFeedback : NSObject  {
    
    struct Value: Codable {
        var sonuc: String?
        var message: String?
        
        enum BackendError: Error {
            case urlError(reason: String)
            case objectSerialization(reason: String)
        }
    }
    // GET ALL TODOS
    static func endpointForTodos() -> String {
        return GlobalVariables.shared.apiURL + "geribildirim/gonder"
    }
    
    struct Todo: Codable {
        // POST - SAVE
        static func save(Adi: String, Soyadi: String, Konu: String, Mesaj: String, FirmaId: Int, Tel: String, Email: String
            ,completionHandler: @escaping ([String: String]?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            guard let todosURL = URL(string: endpoint) else {
                let error = Json.BackendError.urlError(reason: "Could not create URL")
                completionHandler(nil, error)
                return
            }
            
            var todosUrlRequest = URLRequest(url: todosURL)
            todosUrlRequest.httpMethod = "POST"
            
            todosUrlRequest.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            var params =
                ["Adi": Adi
                    , "Soyadi":Soyadi
                    , "Konu":Konu
                    , "Mesaj":Mesaj
                    , "Tel":Tel
                    , "Email":Email
                    ] as [String : Any]
            
            if FirmaId != 0 {
                params =
                    ["Adi": Adi
                        , "Soyadi":Soyadi
                        , "Konu":Konu
                        , "Mesaj":Mesaj
                        , "FirmaId": FirmaId
                        , "Tel":Tel
                        , "Email":Email
                    ] as [String : Any]
            }
            
            guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
                let error = Json.BackendError.urlError(reason: "Error httpBody")
                completionHandler(nil, error)
                return
            }
            
            todosUrlRequest.httpBody = httpBody
            
            let session = URLSession.shared
            session.dataTask(with: todosUrlRequest) { (data, response, error) in
                if let response = response {
                    print(response)
                }
                if let data = data {
                    do {
                        let json: [String: String] = try JSONSerialization.jsonObject(with: data, options: []) as! [String : String]
                        
                        completionHandler(json, nil)
                        
                    } catch {
                        completionHandler(nil, error)
                    }
                }
                }.resume()
        }
    }
}
