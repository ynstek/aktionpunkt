//
//  JsonSehir.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 3.02.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonSehir : NSObject  {
    
    struct ValueIL : Codable {
        let Id : Int?
        let Adi : String?
        let PlakaKodu : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            Adi = try values.decodeIfPresent(String.self, forKey: .Adi)
            PlakaKodu = try values.decodeIfPresent(String.self, forKey: .PlakaKodu)
        }
    }
    
    struct ValueILCE : Codable {
        let Id : Int?
        let IlID : Int?
        let Adi : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            IlID = try values.decodeIfPresent(Int.self, forKey: .IlID)
            Adi = try values.decodeIfPresent(String.self, forKey: .Adi)
        }
    }

    
    static func endpointForTodosIL() -> String {
        var url = GlobalVariables.shared.apiURL + "iller/ilListesi"
        
        url = url.replacingOccurrences(of: " ", with: "%20")
        return url
    }
    
    static func endpointForTodosILCE(id: Int) -> String {
        var url = GlobalVariables.shared.apiURL + "iller/ilceListesi?id=\(id)"
        
        url = url.replacingOccurrences(of: " ", with: "%20")
        return url
    }
    
    struct Todo: Codable {
        // GET ALL TODOS
        static func allTodosIL(completionHandler: @escaping ([ValueIL]?, Error?) -> Void) {
            let endpoint = endpointForTodosIL()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                //                let statuscode = (response as! HTTPURLResponse).statusCode
                
                guard let responseData = data else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    let todos = try JSONDecoder().decode([ValueIL].self, from: responseData)
                    
                    completionHandler(todos, nil)
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
        
        // GET ALL TODOS
        static func allTodosILCE(id: Int,completionHandler: @escaping ([ValueILCE]?, Error?) -> Void) {
            let endpoint = endpointForTodosILCE(id: id)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                //                let statuscode = (response as! HTTPURLResponse).statusCode
                
                guard let responseData = data else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    //                    let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                    //                    print(json)
                    let todos = try JSONDecoder().decode([ValueILCE].self, from: responseData)
                    
                    completionHandler(todos, nil)
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
}
