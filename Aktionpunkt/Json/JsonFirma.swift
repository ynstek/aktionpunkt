//
//  JsonFirma.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 12.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonFirma : NSObject  {
    
    struct Value : Codable {
        var Id : Int?
        var Adi : String?
        var Adres : String?
        var Logo : String?
        var Aciklama : String?
        var Mail : String?
        var Telefon : String?
        var Sifre : String?
        var Sirasi : Int?
        var EklenmeTarihi : String?
        var KategoriFirmalaris : [String]?
        var Urunlers : [String]?
        var FirmaIli : String?
        var FirmaIlcesi : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            Adi = try values.decodeIfPresent(String.self, forKey: .Adi)
            Adres = try values.decodeIfPresent(String.self, forKey: .Adres)
            Logo = try values.decodeIfPresent(String.self, forKey: .Logo)
            Aciklama = try values.decodeIfPresent(String.self, forKey: .Aciklama)
            Mail = try values.decodeIfPresent(String.self, forKey: .Mail)
            Telefon = try values.decodeIfPresent(String.self, forKey: .Telefon)
            Sifre = try values.decodeIfPresent(String.self, forKey: .Sifre)
            Sirasi = try values.decodeIfPresent(Int.self, forKey: .Sirasi)
            EklenmeTarihi = try values.decodeIfPresent(String.self, forKey: .EklenmeTarihi)
            KategoriFirmalaris = try values.decodeIfPresent([String].self, forKey: .KategoriFirmalaris)
            Urunlers = try values.decodeIfPresent([String].self, forKey: .Urunlers)
            FirmaIli = try values.decodeIfPresent(String.self, forKey: .FirmaIli)
            FirmaIlcesi = try values.decodeIfPresent(String.self, forKey: .FirmaIlcesi)
        }
    }
    
    static func endpointForTodos(_ kategoriId: Int) -> String {
        var url = ""
        if kategoriId == 0 {
            url = GlobalVariables.shared.apiURL + "firma/firmaList"
        } else {
            url = GlobalVariables.shared.apiURL + "firma/firmaList?kategoriID=\(kategoriId)"
        }
        
        url = url.replacingOccurrences(of: " ", with: "%20")
        return url
    }
    
    struct Todo: Codable {
        // GET ALL TODOS
        static func allTodos(_ kategoriId: Int, completionHandler: @escaping ([Value]?, Error?) -> Void) {
            let endpoint = endpointForTodos(kategoriId)
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                //                let statuscode = (response as! HTTPURLResponse).statusCode
                
                guard let responseData = data else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    let todos = try JSONDecoder().decode([Value].self, from: responseData)
                    
                    completionHandler(todos, nil)
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
}

