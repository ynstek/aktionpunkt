//
//  JsonSlider.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 20.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import Foundation

class JsonSlider : NSObject  {
    
    struct Value : Codable {
        let Id : Int?
        let FirmaID : Int?
        let UrunID : Int?
        let Aciklama : String?
        let ResimYolu : String?
        let Sirasi : Int?
        let EklemeTarihi : String?
        let Firmalar : String?
        let Urunler : String?
        
        init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            Id = try values.decodeIfPresent(Int.self, forKey: .Id)
            FirmaID = try values.decodeIfPresent(Int.self, forKey: .FirmaID)
            UrunID = try values.decodeIfPresent(Int.self, forKey: .UrunID)
            Aciklama = try values.decodeIfPresent(String.self, forKey: .Aciklama)
            ResimYolu = try values.decodeIfPresent(String.self, forKey: .ResimYolu)
            Sirasi = try values.decodeIfPresent(Int.self, forKey: .Sirasi)
            EklemeTarihi = try values.decodeIfPresent(String.self, forKey: .EklemeTarihi)
            Firmalar = try values.decodeIfPresent(String.self, forKey: .Firmalar)
            Urunler = try values.decodeIfPresent(String.self, forKey: .Urunler)
        }
    }
    
    static func endpointForTodos() -> String {
        var url = GlobalVariables.shared.apiURL + "slider/sliderlist"
        
        url = url.replacingOccurrences(of: " ", with: "%20")
        return url
    }
    
    struct Todo: Codable {
        // GET ALL TODOS
        static func allTodos(completionHandler: @escaping ([Value]?, Error?) -> Void) {
            let endpoint = endpointForTodos()
            guard let url = URL(string: endpoint) else {
                print("Error: cannot create URL")
                AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Could not construct URL")
                let error = Json.BackendError.urlError(reason: "Could not construct URL")
                completionHandler(nil, error)
                return
            }
            let urlRequest = URLRequest(url: url)
            
            let session = URLSession.shared
            
            let task = session.dataTask(with: urlRequest) {(data, response, error) in
                //                let statuscode = (response as! HTTPURLResponse).statusCode
                
                guard let responseData = data else {
                    print("Error: did not receive data", error!)
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: "Did not receive data\n" + error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                guard error == nil else {
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error!.localizedDescription)
                    completionHandler(nil, error)
                    return
                }
                
                do {
                    let todos = try JSONDecoder().decode([Value].self, from: responseData)
                    
                    completionHandler(todos, nil)
                    
                } catch {
                    print("Error trying to convert data to JSON")
                    AlertFunctions.messageType.showOKAlert("Error", bodyMessage: error.localizedDescription)
                    print(error)
                    completionHandler(nil, error)
                }
            }
            task.resume()
        }
    }
}


