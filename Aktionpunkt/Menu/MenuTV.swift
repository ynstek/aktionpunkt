//
//  MenuTV.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 14.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class MenuTV: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let cell = tableView.cellForRow(at: indexPath) as! MenuTViewCell
        
        if indexPath.row == 1 { // Favorilerim
            GlobalVariables.shared.isFavorilerim = true
        } else if indexPath.row == 3 { // Feedback
            
        } else if indexPath.row == 4 { // Shared
//            let appname = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
            let myWebsite = NSURL(string: "http://itunes.apple.com/app/" + "sakarya-kampanyalar/id1347087013")
            guard let url = myWebsite else {
                print("nothing found")
                return
            }
            
            let shareItems:Array = ["Aktionpunkt.at\n", url] as [Any]
            let activityViewController:UIActivityViewController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
            activityViewController.excludedActivityTypes = [UIActivityType.print, UIActivityType.postToWeibo, UIActivityType.copyToPasteboard, UIActivityType.addToReadingList, UIActivityType.postToVimeo]
            self.present(activityViewController, animated: true, completion: nil)
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
		self.backButton()
		
        if segue.identifier == "feedback" {
            let vc = segue.destination as! FeedbackVC
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                vc.image.image = UIImage(named: "logo")
            }
        }
        
    }
    
}
