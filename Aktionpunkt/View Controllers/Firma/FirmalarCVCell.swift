//
//  FirmalarCVCell.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 12.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class FirmalarCVCell: UICollectionViewCell {
    
    @IBOutlet weak var adi: UILabel!
    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var imgFav: UIImageView!
    
    var isFav = false
    var firma : JsonFirma.Value? = nil

    @IBAction func btnFav(_ sender: Any) {
        var favs: [JsonFirma.Value] = []
        
        if let data = UserDefaults.standard.value(forKey: "Favorilerim") as? Data {
            UserDefaults.standard.removeObject(forKey: "Favorilerim")
            UserDefaults.standard.synchronize()
            favs = try! PropertyListDecoder().decode(Array<JsonFirma.Value>.self, from: data)
        }
        
        if isFav {
            imgFav.image = UIImage(named: "fav0")
            
            let index = favs.index{ $0.Id == firma!.Id}
            if let index = index {
                favs.remove(at: index)
            }
            
        } else {
            imgFav.image = UIImage(named: "fav1")
            favs.append(firma!)
        }
        
        GlobalVariables.shared.jsonFavorilerim = favs
        
        UserDefaults.standard.set(try? PropertyListEncoder().encode(favs), forKey: "Favorilerim")
        UserDefaults.standard.synchronize()
        
        isFav = !isFav
    }
    
}
