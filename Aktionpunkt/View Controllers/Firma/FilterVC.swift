//
//  FilterVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 2.02.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class FilterVC: APViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var lblil: UILabel!
    @IBOutlet weak var lblilce: UILabel!
    
    @IBOutlet weak var pickeril: UIPickerView!
    @IBOutlet weak var pickerilce: UIPickerView!
//
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let il = UserDefaults.standard.object(forKey: "IL") as? String {
            self.lblil.text = il
        }
        
        if let ilce = UserDefaults.standard.object(forKey: "ILCE") as? String {
            self.lblilce.text = ilce
        }
        self.getIL()
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "reloadFirma"), object: nil)
    }
    
    @IBAction func clearFilter(_ sender: Any) {
        
        // Remove
        self.lblil.text = ""
        UserDefaults.standard.removeObject(forKey: "IL")
        UserDefaults.standard.synchronize()
        
        // Remove
        self.lblilce.text = ""
        UserDefaults.standard.removeObject(forKey: "ILCE")
        UserDefaults.standard.synchronize()
        
        self.getILCE(id: 0)
    }
    
    func getIL() {
        JsonSehir.Todo.allTodosIL() { (result, error) in
            if error == nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalVariables.shared.jsonIL = result!
                    self.pickeril.reloadAllComponents()

                    if let il = UserDefaults.standard.object(forKey: "IL") as? String {
                        self.lblil.text = il
//                        if let row = GlobalVariables.shared.jsonIL.index
//                        pickeril.selectRow(il, inComponent: 0, animated: false)
                    
                    } else {
                        self.lblil.text = result![0].Adi
                        // Save
                        UserDefaults.standard.set(result![0].Adi, forKey: "IL")
                        UserDefaults.standard.synchronize()
                        // Remove
                        UserDefaults.standard.removeObject(forKey: "ILCE")
                        UserDefaults.standard.synchronize()
                    }
                    
                    if let ilce = UserDefaults.standard.object(forKey: "ILCE") as? String {
                        self.lblilce.text = ilce
                    } else {
                        self.lblilce.text = ""
                    }
                    
                }
            }
        }
    }

    func getILCE(id: Int) {
        self.lblilce.text = ""
        JsonSehir.Todo.allTodosILCE(id: id) { (result, error) in
            if error == nil {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    GlobalVariables.shared.jsonILCE = result!
                    self.pickerilce.reloadAllComponents()
                    if result?.count != 0 {
                        self.lblilce.text = result![0].Adi
                        
                        // Save
                        UserDefaults.standard.set(result![0].Adi, forKey: "ILCE")
                        UserDefaults.standard.synchronize()
                    }
                }
            }
        }
    }

    // MARK:- PickerView Delegate & DataSource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickeril {
            return GlobalVariables.shared.jsonIL.count
        } else if pickerView == pickerilce {
            return GlobalVariables.shared.jsonILCE.count
        }
        return 0
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickeril {
            return GlobalVariables.shared.jsonIL[row].Adi
        } else if pickerView == pickerilce {
            return GlobalVariables.shared.jsonILCE[row].Adi
        }
        return nil
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickeril {
            if GlobalVariables.shared.jsonIL.count != 0 {
                self.lblil.text = GlobalVariables.shared.jsonIL[row].Adi
                // Save
                UserDefaults.standard.set(GlobalVariables.shared.jsonIL[row].Adi, forKey: "IL")
                UserDefaults.standard.synchronize()
                
                self.getILCE(id: GlobalVariables.shared.jsonIL[row].Id!)
                
            }
        } else if pickerView == pickerilce {
            if GlobalVariables.shared.jsonILCE.count != 0 {
                self.lblilce.text = GlobalVariables.shared.jsonILCE[row].Adi
                // Save
                UserDefaults.standard.set(GlobalVariables.shared.jsonILCE[row].Adi, forKey: "ILCE")
                UserDefaults.standard.synchronize()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
