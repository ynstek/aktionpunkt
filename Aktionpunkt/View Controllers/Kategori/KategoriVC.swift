//
//  KategoriVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 12.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import Firebase


class KategoriVC: APViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {
    
    @IBOutlet weak var viewHeigth: NSLayoutConstraint!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
		self.presentTransparentNavigationBar()
        self.getSlider()
        self.getKategories()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.leftBarButtonItem =
            GlobalVariables.shared.newBarButton("menu", action: #selector(SWRevealViewController.revealToggle(_:)), view: self.revealViewController())
        
        GlobalVariables.menuView(current: self)
    }
    
    func getSlider() {
        if Reachability.isConnectedToNetwork() {
            GlobalVariables.openActivity()
            GlobalVariables.shared.jsonSlider.removeAll()
            JsonSlider.Todo.allTodos() { (result, error) in
                if error == nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.shared.jsonSlider = result!
                        self.fillScrollView()
                        GlobalVariables.closeActivity()
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.closeActivity()
                    }
                }
            }
        }
    }
    
    func fillScrollView()
    {
        self.scrollView.isPagingEnabled = true
        var countIndex = CGFloat(0)
        
        for list in GlobalVariables.shared.jsonSlider {
            let image = UIImageView()
            image.frame = CGRect(x: countIndex * view.frame.width, y: 0, width: view.frame.width, height: self.scrollView.frame.height)
            image.image = nil
            if let imageUrl = list.ResimYolu {
                DispatchQueue.main.async {
                    image.downloadedFrom(link: imageUrl)
                }
            }
            
            countIndex += 1
            image.contentMode = .scaleAspectFill
            self.scrollView.addSubview(image)
        }
        
        self.scrollView.contentSize = CGSize(width: CGFloat(countIndex) * view.bounds.size.width, height: scrollView.frame.height)
        
        // PageControl
        self.pageControl.numberOfPages = GlobalVariables.shared.jsonSlider.count
        if self.pageControl.numberOfPages <= 1 {
            self.pageControl.isHidden = true
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
    func getKategories() {
        if Reachability.isConnectedToNetwork() {
            GlobalVariables.openActivity()
            GlobalVariables.shared.jsonKategoriler.removeAll()
            JsonKategori.Todo.allTodos() { (result, error) in
                if error == nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.shared.jsonKategoriler = result!
                        self.collectionView.reloadData()
                        GlobalVariables.closeActivity()
                        
                        // TODO: Firebase - Abone olma uygulama acildiktan sonra yapilmali. ilk baslarda calistirilirsa null veriyor ve abone olamiyor.
                        Messaging.messaging().subscribe(toTopic: "news")
                        print("news abonesi olundu.")
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.closeActivity()
                    }
                }
            }
        }
    }

    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        // MARK: View Size
        let sc = self.scrollView.frame.height
        let count = CGFloat(GlobalVariables.shared.jsonKategoriler.count)
        let csize: CGFloat = 180 + 11
        var size = sc + (count / 2 * csize)
        if size < self.view.frame.height {
            size = self.view.frame.height
        }
        
        self.viewHeigth.constant = sc + (count / 2 * csize)

        return CGSize(width: (self.view.frame.width - 24) / 2, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return GlobalVariables.shared.jsonKategoriler.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! KategorilerCVCell
        
        cell.arkaPlanResmi.image = nil
        cell.resmi.image = nil
        
        cell.adi.text = GlobalVariables.shared.jsonKategoriler[indexPath.row].Adi
        cell.arkaPlanResmi.downloadedFrom(link: GlobalVariables.shared.jsonKategoriler[indexPath.row].ArkaPlanResmi!)
        cell.resmi.downloadedFrom(link: GlobalVariables.shared.jsonKategoriler[indexPath.row].Resmi!)
        
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)
        let cell = sender as! UICollectionViewCell
        let indexPath = self.collectionView!.indexPath(for: cell)!
        
        let vc = segue.destination as! FirmalarVC
        vc.kategoriId = GlobalVariables.shared.jsonKategoriler[indexPath.row].Id!
        GlobalVariables.shared.isFavorilerim = false
    }
    
}
