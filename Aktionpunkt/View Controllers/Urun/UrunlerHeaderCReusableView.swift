//
//  UrunlerHeaderCReusableView.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 13.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class UrunlerHeaderCReusableView: UICollectionReusableView {
    @IBOutlet weak var firmaLogo: UIImageView!
    @IBOutlet weak var firmaAdi: UILabel!
    @IBOutlet weak var firmaAdres: UILabel!
    @IBOutlet weak var firmaTelefon: UILabel!
    
 
    @IBAction func btnPhone(_ sender: Any) {
        let phone = self.firmaTelefon.text!.replacingOccurrences(of: " ", with: "")
        GlobalVariables.openUrl("tel://" + phone)
    }
    
    @IBAction func btnAdress(_ sender: Any) {
        let addressString: String = "http://maps.apple.com/?q=" + self.firmaAdres.text!.replaceTr(text: self.firmaAdres.text!)
        
        GlobalVariables.openUrl(addressString)
    }
    
}
