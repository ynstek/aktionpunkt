//
//  UrunCVCell.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 13.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class UrunlerCVCell: UICollectionViewCell {
    
    @IBOutlet weak var urunResmi: UIImageView!
    @IBOutlet weak var adi: UILabel!
    @IBOutlet weak var fiyat: UILabel!
    @IBOutlet weak var birimFiyat: UILabel!
    
    @IBOutlet weak var indirimOrani: UILabel!
}
