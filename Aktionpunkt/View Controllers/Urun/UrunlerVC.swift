//
//  UrunlerVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 13.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class UrunlerVC: APViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var firmaID = 0
    @IBOutlet weak var collectionView: UICollectionView!
    var firma: JsonFirma.Value? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        fillFirma()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GlobalVariables.menuView(current: self)
    }
    
    
    func fillFirma() {
        self.getUrunler()
    }
    
    func getUrunler() {
        if Reachability.isConnectedToNetwork() {
            GlobalVariables.openActivity()
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
            JsonUrun.Todo.allTodos(self.firmaID) { (result, error) in
                if error == nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.shared.jsonUrunler = result!
                        self.collectionView.reloadData()
                        GlobalVariables.closeActivity()
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.closeActivity()
                    }
                }
            }
        }
    }
    
    // MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width - 24) / 2, height: 180)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
        
        case UICollectionElementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "HeaderCell", for: indexPath) as! UrunlerHeaderCReusableView
            
            if GlobalVariables.shared.isFavorilerim {
                firma = GlobalVariables.shared.jsonFavorilerim.filter() {
                    firmalar in
                    return firmalar.Id == self.firmaID
                    }[0]
            } else {
                firma = GlobalVariables.shared.jsonFirmalar.filter() {
                    firmalar in
                    return firmalar.Id == self.firmaID
                    }[0]
            }
            
            if firma != nil {
                headerView.firmaAdi.text = firma!.Adi
                headerView.firmaTelefon.text = firma!.Telefon
                headerView.firmaAdres.text = firma!.Adres
                
                headerView.firmaLogo.image = UIImage(named: "noimage")
                headerView.firmaLogo.downloadedFrom(link: firma!.Logo!)
            }
            
            return headerView
        default:
//            assert(false, "Unexpected element kind")
            fatalError("Unexpected element kind")
            
        }
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return GlobalVariables.shared.jsonUrunler.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! UrunlerCVCell
        
        cell.urunResmi.image = UIImage(named: "noimage")
        if let urunResmi = GlobalVariables.shared.jsonUrunler[indexPath.row].urunResmi {
            cell.urunResmi.downloadedFrom(link: urunResmi)
        }
        
        cell.fiyat.text = ""
        cell.birimFiyat.attributedText = GlobalVariables.shared.strikeout("")
        cell.adi.text = ""
        cell.indirimOrani.isHidden = true
        cell.indirimOrani.text = ""
        
        cell.adi.text = GlobalVariables.shared.jsonUrunler[indexPath.row].adi
        let paraBirimi = GlobalVariables.shared.jsonUrunler[indexPath.row].paraBirimi!
        
        if GlobalVariables.shared.jsonUrunler[indexPath.row].indirimliMi! {
            
            var birimfiyat = ""
            if let f = GlobalVariables.shared.jsonUrunler[indexPath.row].birimFiyat {
                birimfiyat = String(f)
            }
            
            let stringValue = paraBirimi + " " + birimfiyat
            
            cell.birimFiyat.attributedText = GlobalVariables.shared.strikeout(stringValue)
            birimfiyat = ""
            if let f = GlobalVariables.shared.jsonUrunler[indexPath.row].indirimliFiyat {
                birimfiyat = String(f)
            }
            cell.fiyat.text = paraBirimi + " " + birimfiyat
            
            cell.indirimOrani.isHidden = false
            cell.indirimOrani.text = "-%" + String(GlobalVariables.shared.jsonUrunler[indexPath.row].indirimOrani!)
            
        } else {
            var birimfiyat = ""
            if let f = GlobalVariables.shared.jsonUrunler[indexPath.row].birimFiyat {
                birimfiyat = String(f)
            }
            
            cell.fiyat.text = paraBirimi + " " + birimfiyat
        }
        return cell
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "urundetay" {
            let cell = sender as! UICollectionViewCell
            let indexPath = self.collectionView!.indexPath(for: cell)!
            let vc = segue.destination as! UrunDetayVC
            vc.urunId = GlobalVariables.shared.jsonUrunler[indexPath.row].id!
        } else if segue.identifier == "geribildirim" {
            let vc = segue.destination as! FeedbackVC
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                vc.image.image = UIImage(named: "noimage")
                vc.image.downloadedFrom(link: self.firma!.Logo!)
            }
            vc.firmaId = self.firmaID
        }
            
    }
    
}
