//
//  UrunDetayVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 13.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import WebKit

class UrunDetayVC: APViewController, WKNavigationDelegate, UIWebViewDelegate, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var adi: UILabel!
    
    @IBOutlet weak var WebViewAciklama: UIWebView!
    @IBOutlet weak var birimFiyat: UILabel!
    @IBOutlet weak var fiyat: UILabel!
    @IBOutlet weak var indirimOrani: UILabel!

    @IBOutlet weak var scrollViewHeight: NSLayoutConstraint!
    
    var urunId = 0
    var urun: JsonUrun.Value? = nil

    @IBOutlet weak var pageControl: UIPageControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        WebViewAciklama.delegate = self
        fillUrun()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GlobalVariables.menuView(current: self)
    }

    func fillUrun () {
        self.fiyat.text = ""
        self.birimFiyat.attributedText = GlobalVariables.shared.strikeout("")
        self.adi.text = ""
        WebViewAciklama.loadHTMLString("", baseURL: nil)
        self.indirimOrani.isHidden = true
        
        
        urun = GlobalVariables.shared.jsonUrunler.filter() {
            urunler in
            return urunler.id == self.urunId
            }[0]
        
        if urun != nil {
            self.adi.text = urun!.adi
            WebViewAciklama.loadHTMLString(urun!.uzunAciklama!, baseURL: nil)
            
            let paraBirimi = urun!.paraBirimi!
            if urun!.indirimliMi! {
                var birimfiyat = ""
                if let f = urun!.birimFiyat {
                    birimfiyat = String(f)
                }
                let stringValue = paraBirimi + " " + birimfiyat
                self.birimFiyat.attributedText = GlobalVariables.shared.strikeout(stringValue)
                
                self.fiyat.text = paraBirimi + " " + String(urun!.indirimliFiyat!)
                self.indirimOrani.isHidden = false
                self.indirimOrani.text = "-%" + String(urun!.indirimOrani!)
                
            } else {
                var birimfiyat = ""
                if let f = urun!.birimFiyat {
                    birimfiyat = String(f)
                }
                
                self.fiyat.text = paraBirimi + " " + birimfiyat
            }
            
            fillScrollView()
        }
    }
    
    // WebView
    func insertCSSStringg(into webView: UIWebView) {
        let cssString = "img{ width:100%; height:auto;} body { font-size: 15px }"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        //        webView.evaluateJavaScript(jsString, completionHandler: nil)
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        insertCSSStringg(into: webView) // 1
        webView.frame.size.height = 1
        webView.frame.size = webView.sizeThatFits(.zero)
        self.scrollViewHeight.constant = 369 + webView.frame.size.height
        webView.scrollView.isScrollEnabled = false;

        //        insertContentsOfCSSFile(into: webView) // 2
    }
    
    func fillScrollView()
    {
        self.scrollView.isPagingEnabled = true
        var countIndex = CGFloat(0)

        // Vitrin resmi
        let image = UIImageView()
        image.frame = CGRect(x: countIndex * view.frame.width, y: 0, width: view.frame.width, height: self.scrollView.frame.height)
        image.image = UIImage(named: "noimage")
        if let imageUrl = self.urun!.urunResmi {
            DispatchQueue.main.async {
                image.downloadedFrom(link: imageUrl)
            }
        }
        countIndex += 1
        image.contentMode = .scaleAspectFill
        self.scrollView.addSubview(image)
        
        if let sliderList = self.urun!.urunResimleris {
            for list in sliderList {
                let image = UIImageView()
                image.frame = CGRect(x: countIndex * view.frame.width, y: 0, width: view.frame.width, height: self.scrollView.frame.height)
                image.image = UIImage(named: "noimage")
                if let imageUrl = list.resimYolu {
                    DispatchQueue.main.async {
                        image.downloadedFrom(link: imageUrl)
                    }
                }
                
                countIndex += 1
                image.contentMode = .scaleAspectFill
                self.scrollView.addSubview(image)
            }
        }
        
        self.scrollView.contentSize = CGSize(width: CGFloat(countIndex) * view.bounds.size.width, height: scrollView.frame.height)
        
        // PageControl
        self.pageControl.numberOfPages = self.urun!.urunResimleris!.count + 1
        if self.pageControl.numberOfPages <= 1 {
            self.pageControl.isHidden = true
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
}
