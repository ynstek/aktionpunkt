//
//  IletisimVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 22.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class IletisimVC: APViewController {
    
	@IBOutlet var phoneLabel: UILabel!
	@IBOutlet var adressLabel: UILabel!
	
	override func viewDidLoad() {
        super.viewDidLoad()
        self.phoneLabel.text = "+43 680 1161575"
		self.adressLabel.text = "Fuchsthallergasse 5, 1090 Wien"
    }
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnPhone(_ sender: Any) {
        GlobalVariables.openUrl("tel://+436801161575")
    }
    
    @IBAction func btnAdress(_ sender: Any) {
        let addressString: String = "http://maps.apple.com/?q="
            + "Fuchsthallergasse+5,+1090+Wien";
        // Fuchsthallergasse+5,+1090+Wien
        GlobalVariables.openUrl(addressString)
    }
    
}

