//
//  GizlilikPolitikasiVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 22.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class GizlilikPolitikasiVC: APViewController {
    
    @IBOutlet weak var textField: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

	override func viewWillAppear(_ animated: Bool) {
		self.textField.scrollRangeToVisible(NSMakeRange(0,0))
	}
    
    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}

