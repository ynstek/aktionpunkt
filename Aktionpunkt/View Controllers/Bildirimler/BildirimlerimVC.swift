//
//  BildirimlerimVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 19.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit

class BildirimlerimVC: APViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presentTransparentNavigationBar()
        self.getBildirimler()
    }

    override func viewWillAppear(_ animated: Bool) {
        if AppDelegate.isNofitication {
            self.navigationItem.rightBarButtonItem = GlobalVariables.shared.newBarButton("close", action: #selector(BildirimlerimVC.close), view: self, w: 22, h: 22)
        } else {
            self.navigationItem.leftBarButtonItem =
                GlobalVariables.shared.newBarButton("menu", action: #selector(SWRevealViewController.revealToggle(_:)), view: self.revealViewController())
            
            GlobalVariables.menuView(current: self)
        }
    }
    
    @objc func close () {
        AppDelegate.isNofitication = false
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AnaSayfa")
        GlobalVariables.getTopController().addChildViewController(vc)
        vc.view.frame = GlobalVariables.getTopController().view.frame
        GlobalVariables.getTopController().view.addSubview(vc.view)
        vc.didMove(toParentViewController: GlobalVariables.getTopController())
    }
    
    func getBildirimler() {
        if Reachability.isConnectedToNetwork() {
            GlobalVariables.openActivity()
            GlobalVariables.shared.jsonBildirimler.removeAll()
            JsonBildirimler.Todo.allTodos() { (result, error) in
                if error == nil {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.shared.jsonBildirimler = result!
                        self.tableView.reloadData()
                        GlobalVariables.closeActivity()
                    }
                    
                } else {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        GlobalVariables.closeActivity()
                    }
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return GlobalVariables.shared.jsonBildirimler.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        let data = GlobalVariables.shared.jsonBildirimler[indexPath.row]
        
        cell.textLabel!.text = data.Baslik!
        cell.detailTextLabel?.text = data.EklemeTarihi!.prefix(16).replacingOccurrences(of: "T", with: " ")
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		super.prepare(for: segue, sender: sender)

        let cell = sender as! UITableViewCell
        let indexPath = self.tableView!.indexPath(for: cell)!
        let vc = segue.destination as! BildirimDetayVC
        let data = GlobalVariables.shared.jsonBildirimler[indexPath.row]

        vc.mesaj = data.Mesaj!
        vc.baslik = data.Baslik!
    }
    
}
