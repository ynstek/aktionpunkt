//
//  BildirimDetayVC.swift
//  Aktionpunkt
//
//  Created by Yunus Tek on 22.01.2018.
//  Copyright © 2018 loopbs. All rights reserved.
//

import UIKit
import WebKit

class BildirimDetayVC: APViewController, WKNavigationDelegate, UIWebViewDelegate {

    @IBOutlet weak var txtBaslik: UILabel!
    @IBOutlet weak var WebViewAciklama: UIWebView!
    
    var mesaj = ""
    var baslik = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        WebViewAciklama.delegate = self
        WebViewAciklama.loadHTMLString(mesaj, baseURL: nil)
        txtBaslik.text = baslik
    }

    // WebView
    func insertCSSStringg(into webView: UIWebView) {
        let cssString = "img{ width:100%; height:auto;} body { font-size: 15px }"
        let jsString = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style);"
        //        webView.evaluateJavaScript(jsString, completionHandler: nil)
        webView.stringByEvaluatingJavaScript(from: jsString)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        insertCSSStringg(into: webView) // 1
        
        //        insertContentsOfCSSFile(into: webView) // 2
    }

}
