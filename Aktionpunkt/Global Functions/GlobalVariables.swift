//
//  GlobalVariables.swift
//  Orca POS Mobile
//
//  Created by Yunus TEK on 21.03.2017.
//  Copyright © 2017 Orca Businesss Solutions. All rights reserved.
//

import Foundation
import CoreData
import UIKit

private let _sharedGlobalVariables = GlobalVariables()
final public class GlobalVariables : NSObject  {
    
    // MARK: - SHARED INSTANCE
    class var shared : GlobalVariables {
        return _sharedGlobalVariables
    }
	
	let apiURL = "http://api.aktionspunkt.at/api/"
	
    var isFavorilerim = false
    
    lazy var jsonKategoriler : [JsonKategori.Value] = {
        return [JsonKategori.Value]()
    }()
        
    lazy var jsonFirmalar : [JsonFirma.Value] = {
        return [JsonFirma.Value]()
    }()
    
    lazy var jsonFavorilerim : [JsonFirma.Value] = {
        return [JsonFirma.Value]()
    }()
    
    lazy var jsonUrunler : [JsonUrun.Value] = {
        return [JsonUrun.Value]()
    }()
    
    lazy var jsonBildirimler : [JsonBildirimler.Value] = {
        return [JsonBildirimler.Value]()
    }()
    
    lazy var jsonSlider : [JsonSlider.Value] = {
        return [JsonSlider.Value]()
    }()
    
    lazy var jsonIL : [JsonSehir.ValueIL] = {
        return [JsonSehir.ValueIL]()
    }()
    
    var selectedIL: JsonSehir.ValueIL? = nil
    
    lazy var jsonILCE : [JsonSehir.ValueILCE] = {
        return [JsonSehir.ValueILCE]()
    }()
    
    var selectedILCE: JsonSehir.ValueILCE? = nil
    
    class func menuView(current: UIViewController, right: Bool = false) {
        current.revealViewController().rearViewRevealWidth = 260 // Menu

        if right == false {
            current.revealViewController().rightViewRevealWidth = 0
        } else {
            current.revealViewController().rightViewRevealWidth = 150 // Filter
        }
        current.revealViewController().rightViewRevealOverdraw = 0 // default: 60
        
        current.view.addGestureRecognizer(current.revealViewController().panGestureRecognizer())
    }
    
    class func openUrl(_ string: String) {
        if let url = URL(string: string), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    class func openActivity() {
        // Hidden navigationbar
        getTopController().navigationController?.isNavigationBarHidden = true
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        
        // Open
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ActivityView")
        getTopController().addChildViewController(vc)
        vc.view.frame = getTopController().view.frame
        getTopController().view.addSubview(vc.view)
        vc.didMove(toParentViewController: getTopController())
    }
    
    class func closeActivity() {
        // if it is open, close it
        if let cvc = getTopController().childViewControllers.last {
            cvc.view.backgroundColor = UIColor.clear
            cvc.view.isHidden = true
            cvc.view.alpha = 0
            cvc.willMove(toParentViewController: nil)
            cvc.view.removeFromSuperview()
            cvc.removeFromParentViewController()
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            // Hidden navigationbar
            getTopController().navigationController?.isNavigationBarHidden = false
        }
    }
    
    func newBarButton(_ imgName: String, action: Selector, view: UIViewController, w: Int = 30, h: Int = 22) -> UIBarButtonItem{
        let button: UIButton = UIButton(type: .custom)
        button.setImage(UIImage(named: imgName), for: UIControlState())
//        button.frame = CGRect(x: 20, y: 20, width: w, height: h)
        //        button.bounds = CGRect(x: 0,y: 70,width: 30,height: 80)
        button.bounds = CGRect(x: 20,y: 20,width: w,height: h)

        button.addTarget(view, action: action, for: .touchUpInside)
        let barButton = UIBarButtonItem(customView: button)
        return barButton
    }
    
    class func getTopController() -> UIViewController {
        var topController = UIApplication.shared.keyWindow!.rootViewController! as UIViewController
        while ((topController.presentedViewController) != nil) {
            topController = topController.presentedViewController!;
        }
        return topController
    }
    
    class func isValidEmail(_ email:String) -> Bool {
        print("validate emilId: \(email)")
        
        let emailRegEx =
            "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
                + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
                + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
                + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
                + "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
                + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
                + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        //        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let emailTest = NSPredicate(format:"SELF MATCHES[c] %@", emailRegEx)
        
        let result = emailTest.evaluate(with: email)
        
        if result == false {
            AlertFunctions.messageType.showOKAlert("WARNUNG", bodyMessage: "Sie müssen die richtige E-Mail-Adresse eingeben")
        }
        
        return result
    }
    
    class func isValidPhone(_ value: String) -> Bool {
        
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        var result =  phoneTest.evaluate(with: value)
        
        if value.count != 13 {
            result = false
        }
        
        if result == false {
            AlertFunctions.messageType.showOKAlert("WARNUNG", bodyMessage: "Sie müssen die richtige Telefonnummer eingeben")
        }
        
        return result
    }
    
    func strikeout(_ text:String) -> NSAttributedString {
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.baselineOffset, value: 0, range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.styleThick.rawValue), range: NSMakeRange(0, attributedString.length))
        attributedString.addAttribute(NSAttributedStringKey.strikethroughColor, value: UIColor.gray, range: NSMakeRange(0, attributedString.length))
        
        return attributedString
    }
    
}

extension UIImageView {
    func downloadedFrom(url: URL) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String) {
        guard let url = URL(string: link) else {
            return
        }
        downloadedFrom(url: url)
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround(){
        // Tap
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyBoard)) // yazılan viewin bir önemi yok.
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyBoard(_ sender: UITapGestureRecognizer? = nil){
        view.endEditing(true)
    }
	
	func backButton() {
		navigationController?.navigationBar.topItem?.title = " " // x10 character
//		navigationController?.navigationBar.tintColor = UIColor.white
	}
	
	func presentTransparentNavigationBar() {
		self.navigationController!.navigationBar.setBackgroundImage(UIImage(), for:UIBarMetrics.default)
		self.navigationController!.navigationBar.isTranslucent = true
		self.navigationController!.navigationBar.shadowImage = UIImage()
		self.navigationController!.setNavigationBarHidden(false, animated:true)
		self.navigationItem.hidesBackButton = true
		self.backButton()
		//        self.edgesForExtendedLayout = UIRectEdge.None // Kenarlar
		//        self.navigationController!.navigationBar.barTintColor = GlobalFunctions.shared.getColor("black")
	}
}

extension String {
    func replaceTr(text:String) -> String {
        var stext = text.replacingOccurrences(of: "ğ", with: "g")

        stext = stext.replacingOccurrences(of:"ü", with: "u")
        stext = stext.replacingOccurrences(of:"ş", with: "s")
        stext = stext.replacingOccurrences(of:"ç", with: "c")
        stext = stext.replacingOccurrences(of:"ö", with: "o")
        stext = stext.replacingOccurrences(of:"ı", with: "i")
        stext = stext.replacingOccurrences(of:"â", with: "a")
        stext = stext.replacingOccurrences(of:"û", with: "u")
        stext = stext.replacingOccurrences(of:"Ğ", with: "g")
        stext = stext.replacingOccurrences(of:"Ü", with: "u")
        stext = stext.replacingOccurrences(of:"Ş", with: "s")
        stext = stext.replacingOccurrences(of:"Ç", with: "c")
        stext = stext.replacingOccurrences(of:"Ö", with: "o")
        stext = stext.replacingOccurrences(of:"İ", with: "i")
        stext = stext.replacingOccurrences(of:"î", with: "i")
        
        stext = stext.replacingOccurrences(of:" ", with: "+")
        return stext;
    }
}

extension UIColor {
    static func colorWithRedValue(_ redValue: CGFloat, greenValue: CGFloat, blueValue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: redValue/255.0, green: greenValue/255.0, blue: blueValue/255.0, alpha: alpha)
    }
}
